//initialize
var webviewPreload;
var currentTab = 1;
const {ipcRenderer} = require('electron');
var displayRemoved = new Event('display-removed');
var tabDisplay;
var today = new Date();
var schedulePeriods;
var table;
var scheduleModified = false;
var currentTimes = [];
var tableHeight;
var tableWidth = 1;
var rootElement = document.documentElement;
var tableContent = [['']];
var schedules = {length: 0};
var tabs = {"tab2": ["Google Classroom", "https://classroom.google.com/?emr=0"], "tab3": ["Google Docs", "https://docs.google.com"], "tab4": ["MathXL", "https://mathxlforschool.com"], "tab5": ["Powerschool", "https://rmcacs.powerschool.com"], "length": 4};
//load webviewPreload.js into variable
getFileText('webviewPreload.js');
//check if data is already populated, and, if not, populate data. otherwise, load already existing data
if(!localStorage.getItem('opened')) {
  //populate storage
  populateStorage();
  //set background to american flag on independence day
  if((today.getDate() == 4) && (today.getMonth() == 6)) {
    document.getElementsByTagName('html')[0].style.background = 'url(https://upload.wikimedia.org/wikipedia/en/a/a4/Flag_of_the_United_States.svg) no-repeat center center fixed';
    document.getElementsByTagName('html')[0].style.backgroundColor = 'rgb(118, 146, 191)';
    document.getElementsByTagName('html')[0].style.backgroundSize = 'cover';
  }
  ipcRenderer.send('application-loaded');
  document.getElementById('loadingOverlay').style.visibility = "hidden";
} else {
  //load data
  loadData(false);
  //set background to american flag on independence day
  if((today.getDate() == 4) && (today.getMonth() == 6)) {
    document.getElementsByTagName('html')[0].style.background = 'url(https://upload.wikimedia.org/wikipedia/en/a/a4/Flag_of_the_United_States.svg) no-repeat center center fixed';
    document.getElementsByTagName('html')[0].style.backgroundColor = 'rgb(118, 146, 191)';
    document.getElementsByTagName('html')[0].style.backgroundSize = 'cover';
  }
  ipcRenderer.send('application-loaded');
  document.getElementById('loadingOverlay').style.visibility = "hidden";
}
//button click script
function buttonClick(id, needSwitch) {
  var button = document.getElementById(id);
  //check if button exists, and if so, run animation
  if(button) {
    //switch content if it needs to be switched
    if(id !== 'quitTab' && needSwitch) {
      if(id.substring(3, id.length) == "1") {
        switchContent("home", id);
      } else if(id == 'newTab') {
        switchContent('newTabContent', id);
      } else if(id == 'scheduleTab') {
        switchContent('scheduleContent', id);
      } else {
        switchContent("content"+id.substring(3, id.length), id);
      }
    }
    animateClick(button, needSwitch);
    //quit if quit button pressed
    if(id == 'quitTab') {
      setTimeout(function() {quitSchoolApp();}, 500);
    }
    //modify title header, if needed
    if(needSwitch) {
      var header = document.getElementById('header').children[0];
      var title = document.getElementById(id).children[0];
      header.innerHTML = title.title;
    }
  }
}
//load data from memory
function loadData(populate) {
  //check if data is being populated, and if it is not, reload tab and schedule data
  if(!populate) {
    tabs = JSON.parse(localStorage.getItem('tabs'));
    if(localStorage.getItem('schedules')) {
      schedules = JSON.parse(localStorage.getItem('schedules'));
      for(i = 0; i < schedules.length; i++) {
        window[schedules[i]] = JSON.parse(localStorage.getItem(schedules[i]));
      }
    }
  }
  //create table data for schedule graph
  tableContent[1] = ['Quit SchoolApp'];
  tableContent[2] = ['Home'];
  tableHeight = 3;
  //add tabs from reloaded data
  for(var i=2; i<(tabs.length+2); i++) {
    addTab(tabs["tab"+i.toString()], i, true);
  }
  //add schedules from reloaded data
  for(i = 0; i < schedules.length; i++) {
    generateTable(window[schedules[i]].schedule, 'scheduleTable'+i);
    document.getElementById('noSchedules').style.visibility = 'hidden';
    document.getElementById('noSchedules').style.height = '0px';
    var title = document.createElement('h3');
    title.innerHTML = schedules[i];
    document.getElementsByClassName('saveSchedules')[0].appendChild(title);
    document.getElementsByClassName('saveSchedules')[0].appendChild(window['scheduleTable'+i]);
    if(window[schedules[i]].dayDate == 'date') {
      if(getFullDate() == window[schedules[i]].date) {
        var x = window[schedules[i]].times;
        break;
      }
    } else {
      if(today.getDay() == window[schedules[i]].day) {
        var x = window[schedules[i]].times;
      }
    }
  }
  //generate tab display in home menu
  generateTabDisplay();
  document.getElementById('tabDisplay').appendChild(tabDisplay);
  //start schedule
  if(x) {
    startSchedule(x);
  }
  //disable hidden content
  $('.hidden-content').attr('disabled',true);
  setTimeout(function(){document.getElementById('quitTab').children[0].focus();}, 500);
}
//populate storage
function populateStorage() {
  //set storage data
  localStorage.setItem('opened', true);
  localStorage.setItem('tabs', JSON.stringify(tabs));
  //load newly populated data
  loadData(true);
}
//add new tab
function addTab(tabData, tabNo, load) {
  //load tab data and append it to tab list
  var tabEl = document.createElement('li');
  tabEl.id = "tab"+tabNo.toString();
  tabEl.classList.add('generatedTabs');
  tabEl.innerHTML = '<button onclick="buttonClick(\''+tabEl.id+'\', true)" title="'+tabData[0]+'"><img src="https://icons.duckduckgo.com/ip2/'+getDomain(tabData[1].substring(8, tabData[1].length))+'.ico" class="tabImg" />  '+shorten(tabData[0]);
  document.getElementsByTagName('ul')[0].appendChild(tabEl);
  //create new div & iframe for tab and append them to content container
  var divEl = document.createElement('div');
  var ifrm = document.createElement('webview');
  divEl.classList.add('hidden-content', 'displays');
  divEl.id = "content"+tabNo.toString();
  ifrm.src = tabData[1];
  ifrm.addEventListener('did-start-loading', function() {
    window['style'+tabNo] = document.createElement('style');
    window['style'+tabNo].innerHTML = ".content:not(#home):not(#newTabContent):not(#scheduleContent)::after {content:\"Loading...\"\; float: left\; position: absolute\; top: 38vh\; left: 20vw\; font-size: 10vw\; z-index: -1\;}";
    document.head.appendChild(window['style'+tabNo]);
    if(tabs.length+1 == tabNo && currentTab == 1) {
      showTabDisplay(true);
    }
  });
  ifrm.addEventListener('did-stop-loading', function(e) {
    window['style'+tabNo].innerHTML = '';
    window['style'+tabNo].remove()
    e.target.executeJavaScript(webviewPreload);
  });
  ifrm.sandbox = "allow-same-origin allow-scripts allow-forms";
  ifrm.width = "100%";
  ifrm.height = "100%";
  ifrm.target = "_self";
  ifrm.id = "ifrm"+tabNo.toString();
  divEl.appendChild(ifrm);
  document.getElementById('content-container').appendChild(divEl);
  if (!load) {
    //add new tab to tabs object
    tabs['tab'+tabNo.toString()] = tabData;
    tabs.length = tabNo-1;
  }
  //add tab to tabSelect in new Schedule form
  var tabOption = document.createElement('option');
  tabOption.value = 'tab'+tabNo.toString();
  tabOption.innerHTML = tabData[0];
  document.getElementById('tabSelect').appendChild(tabOption);
  //add tab to table element for schedule
  tableContent[tabNo+1] = [tabData[0]];
  if(tableWidth > 1) {
    for(i = 0; i < tableWidth-1; i++) {
      tableContent[tabNo+1].push('');
    }
  }
  tableHeight++;
  //generate updated table
  generateTable(tableContent, 'table');
}
//quit SchoolApp
function quitSchoolApp() {
  document.getElementById('overlay').style.visibility = "visible";
  setTimeout(function() {window.close()}, 200);
}
//shorten text
function shorten(str) {
  if(str.substring(0, 7) !== str) {
    return str.substring(0, 7) + "\u2026";
  } else {
    return str;
  }
}
//edit tab
function editTab(tab) {
  animateClick(tab);
}
//animate object clicked
function animateClick(element, needSwitch) {
  var content = document.getElementsByClassName('content');
  //check if button already has animation class
  if(element.classList.contains('buttonAnimation')) {
    element.classList.remove('buttonAnimation');
  }
  //add animation class
  element.classList.add('buttonAnimation');
  //hide content
  content[0].classList.add('hidden-content-button');
  content[0].classList.remove('content');
  //remove animation class and show content after the animation finishes
  setTimeout(function() {
    element.classList.remove('buttonAnimation');
    document.getElementsByClassName('hidden-content-button')[0].classList.add('content');
    document.getElementsByClassName('hidden-content-button')[0].classList.remove('hidden-content-button');
  }, 500);
  //hide old button and show new button, if needed
  if(needSwitch) {
    setTimeout(function() {
      var oldButton = document.getElementsByClassName('active')[0];
      var button = element.children[0];
      var oldImage = oldButton.children[0];
      var image = button.children[0];
      oldButton.classList.remove('active');
      button.classList.add('active');
      oldImage.classList.remove('activeTab');
      oldImage.classList.add('tabImg');
      image.classList.remove('tabImg');
      image.classList.add('activeTab');
    }, 500);
  }
}
//hide old content and show new content
function switchContent(contentId, tabId) {
  var content = document.getElementsByClassName('content');
  var newContent = document.getElementById(contentId);
  content[0].classList.add('hidden-content');
  content[0].classList.remove('content');
  newContent.classList.add('content');
  newContent.classList.remove('hidden-content');
  //disable hidden content and enable shown content
  $('.hidden-content').attr('disabled',true);
  $('.content').attr('disabled', false);
  if(contentId == 'home') {
    showTabDisplay(true);
  } else {
    showTabDisplay(false);
  }
  if(tabId.substring(0, 3) == 'tab') {
    currentTab = tabId.substring(3, tabId.length);
  }
}
//create new tab when new tab form submitted
function newTab() {
  var name = document.getElementById('name').value;
  var url = document.getElementById('url').value;
  if(name && url) {
    addTab([name, url], tabs.length+2, false);
    setTimeout(function(){
      document.getElementById('name').value = '';
      document.getElementById('url').value = '';
      document.getElementById('name').focus();
    }, 100);
    //update home tab display
    removeTabDisplay();
    generateTabDisplay();
    document.getElementById('tabDisplay').appendChild(tabDisplay);
  }
}
//prevent new tab form from reloading document
document.getElementsByTagName('form')[0].addEventListener('submit', function(e){
  e.preventDefault();
});
//prevent new schedule form from reloading document
document.getElementsByTagName('form')[1].addEventListener('submit', function(e){
  e.preventDefault();
});
//prevent new schedule period form from reloading document
document.getElementsByTagName('form')[2].addEventListener('submit', function(e){
  e.preventDefault();
});
//set date for input elements
Date.prototype.toDateInputValue = (function() {
    var local = new Date(this);
    local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
    return local.toJSON().slice(0,10);
});
document.getElementById('datePicker').children[0].defaultValue = new Date().toDateInputValue();
//change new schedule form when dayDateSelect changed
document.getElementById('dayDateSelect').addEventListener('change', function(e){
  if(e.target.value == 'day') {
    document.getElementById('daySelect').classList.remove('hidden-content');
    document.getElementById('datePicker').classList.add('hidden-content');
    $(document.getElementById('daySelect').children[0]).attr('required', '');
    $(document.getElementById('datePicker').children[0]).removeAttr('required');
  } else if(e.target.value == 'date') {
    document.getElementById('daySelect').classList.add('hidden-content');
    document.getElementById('datePicker').classList.remove('hidden-content');
    $(document.getElementById('datePicker').children[0]).attr('required', '');
    $(document.getElementById('daySelect').children[0]).removeAttr('required');
  } else {
    document.getElementById('daySelect').classList.add('hidden-content');
    document.getElementById('datePicker').classList.add('hidden-content');
    $(document.getElementById('datePicker').children[0]).removeAttr('required');
    $(document.getElementById('daySelect').children[0]).removeAttr('required');
  }
});
//change new schedule time period form when tab picker changed
document.getElementById('tabSelect').addEventListener('change', function(e){
  if(e.target.value == 'quit') {
    document.getElementById('scheduleTimeTo').parentNode.style.visibility = 'hidden';
    $(document.getElementById('scheduleTimeTo')).removeAttr('required');
    document.getElementById('scheduleTimeFrom').parentNode.children[0].innerHTML = 'At:';
  } else {
    document.getElementById('scheduleTimeTo').parentNode.style.visibility = 'visible';
    $(document.getElementById('scheduleTimeTo')).attr('required', '');
    document.getElementById('scheduleTimeFrom').parentNode.children[0].innerHTML = 'From:';
  }
});
//change time value for time input
document.getElementById('scheduleTimeFrom').defaultValue = getTime().replace(':0', ':00').replace('60', '00').replace('010:', '10:');
document.getElementById('scheduleTimeTo').defaultValue = getTime(1).replace(':0', ':00').replace('60', '00').replace('010:', '10:');
//round x to the nearest y
function roundToTheNearest(x, y) {
  return Math.round(x/y)*y;
}
//get current time plus x hours
function getTime(x) {
  if(x){
    if((today.getHours()+x) < 10) {
      if(roundToTheNearest(today.getMinutes(), 15) == 60 && today.getMinutes() < 60) {
        return '0'+(today.getHours()+1+x)+":"+roundToTheNearest(today.getMinutes(), 15);
      } else {
        return '0'+(today.getHours()+x)+":"+roundToTheNearest(today.getMinutes(), 15);
      }
    } else {
      if(roundToTheNearest(today.getMinutes(), 15) == 60 && today.getMinutes() < 60) {
        return today.getHours()+x+1+":"+roundToTheNearest(today.getMinutes(), 15);
      } else {
        return today.getHours()+x+":"+roundToTheNearest(today.getMinutes(), 15);
      }
    }
  } else {
    if(today.getHours() < 10) {
      if(roundToTheNearest(today.getMinutes(), 15) == 60 && today.getMinutes() < 60) {
        return '0'+(today.getHours()+1)+":"+roundToTheNearest(today.getMinutes(), 15);
      } else {
        return '0'+today.getHours()+":"+roundToTheNearest(today.getMinutes(), 15);
      }
    } else {
      if(roundToTheNearest(today.getMinutes(), 15) == 60 && today.getMinutes() < 60) {
        return today.getHours()+1+":"+roundToTheNearest(today.getMinutes(), 15);
      } else {
        return today.getHours()+":"+roundToTheNearest(today.getMinutes(), 15);
      }
    }
  }
}
//add schedule period
function newTimePeriod() {
  var from = document.getElementById('scheduleTimeFrom');
  var to = document.getElementById('scheduleTimeTo');
  var name = document.getElementById('scheduleTimeName');
  var tab = document.getElementById('tabSelect');
  if(validTime(from.value, to.value, tab.value, name.value)) {
    if(from.value && to.value && tab.value && name.value) {
      document.getElementsByTagName('h4')[0].parentNode.style.visibility = 'hidden';
      document.getElementsByTagName('h4')[0].parentNode.style.height = '0px';
      addTimePeriod(from.value, to.value, tab.value, name.value);
      generateTable(tableContent, 'table');
      if(!scheduleModified) {
        document.getElementById('scheduleTableContainer').appendChild(table);
        scheduleModified = true;
      }
    } else if(from.value && tab.value && name.value) {
      document.getElementsByTagName('h4')[0].parentNode.style.visibility = 'hidden';
      document.getElementsByTagName('h4')[0].parentNode.style.height = '0px';
      addTimePeriod(from.value, 0, tab.value, name.value);
      generateTable(tableContent, 'table')
      if(!scheduleModified) {
        document.getElementById('scheduleTableContainer').appendChild(table);
        scheduleModified = true;
      }
    }
  }
  setTimeout(function(){
    document.getElementById('tabSelect').value = '';
    document.getElementById('scheduleTimeName').value = '';
    document.getElementById('scheduleTimeName').focus();
  }, 100);
}
//add time period to table
function addTimePeriod(from, to, tab, name) {
  addTableTime(from, to, tab, name);
}
//add time to table
function addTableTime(from, to, tab, name) {
  var timeAmount;
  var c = tableWidth;
  if(scheduleModified) {
    for(i = 0; i < tableContent[0].length; i++) {
      if(from+' - '+to < tableContent[0][i]) {
        c = i+1;
        break;
      }
    }
  }
  tableContent[0].splice(c, 0, from+' - '+to);
  for(i = 1; i < tableHeight; i++) {
    if(i == (Number(tab.substring(3, tab.length))+1)) {
      tableContent[i].splice(c, 0, name)
    } else {
      tableContent[i].splice(c, 0, '');
    }
  }
  tableWidth++;
  rootElement.style.setProperty('--table-width', (tableWidth*12)+'vw')
}
//check if time period is valid with current schedule
function validTime(from, to, tab, name) {
  //check if the times exist
  if(!(from && to)) {
    return false;
  }
  //check if the first time is after the second
  if(from > to) {
    return false;
  }
  //check if the times are the same
  if(from == to) {
    return false;
  }
  //loop through current times to check if any times overlap
  for (i = 0; i < currentTimes.length; i++) {
    if(((from < currentTimes[i]) && (to > currentTimes[i])) && ((i%3 == 0) || ((i-1)%3 == 0))) {
      return false;
    }
    //check if time period already exists
    if(((i%3 == 0) && (currentTimes[i] == from)) || ((((i-1)%3 == 0) && (currentTimes[i] == to)))) {
      return false;
    }
  }
  //if all checks pass, add new times to time array and return true
  currentTimes.push(from);
  currentTimes.push(to);
  currentTimes.push([tab, name]);
  return true;
}
//generate a table from an array
function generateTable(array, table) {
  //check if table already exists, and if not, create a new one. otherwise, reset the existing table
  if(!window[table]) {
    window[table] = document.createElement('table');
  } else {
    window[table].innerHTML = '';
  }
  //create a new table body element and append it to the table
  var tbody = document.createElement('tbody');
  window[table].appendChild(tbody);
  //loop through table array and create table content
  for(i = 0; i < array.length; i++) {
    //create new table row and append it to the table body
    var tr = document.createElement('tr');
    tbody.appendChild(tr)
    //loop through the table row array and create table data elements
    for(j = 0; j < array[i].length; j++) {
      //create new table data element and append it to the table row
      var td = document.createElement('td');
      td.innerHTML = array[i][j];
      //style table data element based on it's position in the table
      if((i !== 0) && (j !== 0) && array[i][j]) {
        td.classList.add('innerTableActive');
      } else if((i !== 0) && (j !== 0)) {
        td.classList.add('tableInner');
      }
      if((i == 0) && (j > 0)) {
        if(j == 1) {
          td.classList.add('tableTopFirst');
        } else {
          td.classList.add('tableTop');
        }
      }
      if((j == 0) && (i > 0)) {
        if(i == 1) {
          td.classList.add('tableLeftFirst');
        } else {
          td.classList.add('tableLeft');
        }
      }
      tr.appendChild(td);
    }
  }
}
//add schedule
function addSchedule() {
  var name = document.getElementById('scheduleName').value;
  var dayDate = document.getElementById('dayDateSelect').value;
  //check if dayDate is a day of the week or a specific date
  if(dayDate == 'day') {
    var day = document.getElementById('daySelectInner').value;
    //check if the required fields are filled in correctly
    if(name && (day || day == 0) && scheduleModified && !window[name]) {
      //generate a new table in the saved schedules section
      schedules[schedules.length] = name;
      window[name] = {dayDate: 'day', day: day, schedule: tableContent, times: currentTimes};
      generateTable(tableContent, 'scheduleTable'+schedules.length);
      document.getElementById('noSchedules').style.visibility = 'hidden';
      document.getElementById('noSchedules').style.height = '0px';
      var title = document.createElement('h3');
      title.innerHTML = name;
      document.getElementsByClassName('saveSchedules')[0].appendChild(title);
      document.getElementsByClassName('saveSchedules')[0].appendChild(window['scheduleTable'+schedules.length]);
      schedules.length++;
      //if today is a day when the schedule is started, start the schedule
      if(today.getDay() == day) {
        startSchedule(currentTimes);
      }
      //reset the schedule form
      resetScheduleForm();
    }
  } else if(dayDate == 'date') {
    var date = document.getElementById('datePickerInner').value;
    //check if the required fields are filled in correctly
    if(name && date && scheduleModified && !window[name]) {
      //generate a new table in the saved schedules section
      schedules[schedules.length] = name;
      window[name] = {dayDate: 'date', date: date, schedule: tableContent, times: currentTimes};
      generateTable(tableContent, 'scheduleTable'+schedules.length);
      document.getElementById('noSchedules').style.visibility = 'hidden';
      document.getElementById('noSchedules').style.height = '0px';
      var title = document.createElement('h3');
      title.innerHTML = name;
      document.getElementsByClassName('saveSchedules')[0].appendChild(title);
      document.getElementsByClassName('saveSchedules')[0].appendChild(window['scheduleTable'+schedules.length]);
      schedules.length++;
      //if today is a day when the schedule is started, start the schedule
      if(getFullDate() == date) {
        startSchedule(currentTimes);
      }
      //reset the schedule form
      resetScheduleForm();
    }
  }
}
//reset schedule form
function resetScheduleForm() {
  //set timeout to prevent missing data errors
  setTimeout(function(){
    //reset inputs
    document.getElementById('scheduleName').value = '';
    document.getElementById('dayDateSelect').value = '';
    document.getElementById('daySelectInner').value = '';
    document.getElementById('datePickerInner').value = new Date().toDateInputValue();
    //delete generated table
    table.innerHTML = '';
    //show 'no schedule time periods' dialogue
    document.getElementsByTagName('h4')[0].parentNode.style.visibility = 'visible';
    document.getElementsByTagName('h4')[0].parentNode.style.height = '';
    document.getElementById('daySelect').classList.add('hidden-content');
    document.getElementById('datePicker').classList.add('hidden-content');
    $(document.getElementById('datePicker').children[0]).removeAttr('required');
    $(document.getElementById('daySelect').children[0]).removeAttr('required');
    //reset table data
    tableContent = '';
    scheduleModified = false;
    // currentTimes = [];
    document.getElementById('scheduleName').focus();
  }, 100);
}
//get today's full date
function getFullDate() {
  var year = today.getFullYear();
  var month = today.getMonth()+1;
  if(month.toString().length == 1) {
    month = '0'+month;
  }
  var date = today.getDate()
  if(date.toString().length == 1) {
    date = '0'+date;
  }
  return year+'-'+month+'-'+date;
}
//start schedule from array of data
function startSchedule(array) {
  var timeRemaining;
  //reset today
  today = new Date();
  //loop through array of data and start setTimeout for each time period
  for(i = 0; i < array.length; i += 3) {
    //get time between current time and schedule time in milliseconds
    timeRemaining = new Date(today.getFullYear(), today.getMonth(), today.getDate(), getHours(array[i]), getMinutes(array[i]), 0, 0) - today;
    //check if time period has already passed
    if (timeRemaining < 0) {
      buttonClick(array[i+2], true);
      return null; //the time period has already passed, so kill the function
    }
    //start timeout for time period
    setTimeout(buttonClick.bind(null, array[i+2]), timeRemaining);
  }
}
//get hours from an input with type tyme
function getHours(time) {
  return time.substring(0, 2);
}
//get minutes from an input with type time
function getMinutes(time) {
  return time.substring(3, 5);
}
//save all application data
function saveData() {
  saveSchedules();
  saveTabs();
}
//save schedules to file system
function saveSchedules() {
  localStorage.setItem('schedules', JSON.stringify(schedules));
  for(i = 0; i < schedules.length; i++) {
    localStorage.setItem(schedules[i], JSON.stringify(window[schedules[i]]));
  }
}
//save tabs to file system
function saveTabs() {
  localStorage.setItem('tabs', JSON.stringify(tabs));
}
//save data every five minutes
window.setInterval(saveData, 300000);
//add context menu to tabs
$(function() {
  //context menu on generated tabs
  $.contextMenu({
    selector: '.generatedTabs',
    callback: function(key, options) {
      buttonClick(this[0].id, key=="open");
      if(key == 'quit') {
        setTimeout(function() {quitSchoolApp();}, 500);
      } else if(key == 'delete') {
        setTimeout(deleteTab.bind(null, this[0].id.substring(3, this[0].id.length)), 500);
      }
    },
    items: {
      "open": {name: "Open"},
      "delete": {name: "Delete"},
      "sep1": "---------",
      "quit": {name: "Quit"}
    }
  });
  //context menu on default tabs
  $.contextMenu({
    selector: '.defaultTabs',
    callback: function(key, options) {
      buttonClick(this[0].id, key=="open");
      if(key == 'quit') {
        setTimeout(function() {quitSchoolApp();}, 500);
      }
    },
    items: {
      "open": {name: "Open"},
      "sep1": "---------",
      "quit": {name: "Quit"}
    }
  });
  //context menu on tabs in tabdisplay
  $.contextMenu({
    selector: '.overlayContainer',
    callback: function(key, options) {
      var tabId = this[0].parentNode.children[1].id;
      tabId = tabId.substring(7, tabId.length);
      buttonClick('tab'+tabId, key == 'open');
      if(key == 'quit') {
        setTimeout(function() {quitSchoolApp();}, 500);
      } else if(key == 'delete') {
        setTimeout(deleteTab.bind(null, tabId), 500);
      }
    },
    items: {
      "open": {name: "Open"},
      "delete": {name: "Delete"},
      "sep1": "---------",
      "quit": {name: "Quit"}

    }
  });
  //default context menu
  $.contextMenu({
    selector: 'body',
    callback: function(key, options) {
      if(key == "quit") {
        buttonClick('quitTab', false);
        setTimeout(function() {quitSchoolApp();}, 500);
      }
    },
    items: {
      "quit": {name: "Quit"}
    }
  });
});
//generate tab display
function generateTabDisplay() {
  //create document fragment and table
  tabDisplay = document.createDocumentFragment();
  var table = document.createElement('table');
  table.align = "center";
  tabDisplay.appendChild(table);
  var tbody = document.createElement('tbody');
  table.appendChild(tbody);
  var tr;
  var td;
  var overlayContainer;
  var x = 2;
  //loop through tabs array and move content to tab display
  for(i = 0; i < Math.round(tabs.length/2); i++) {
    tr = document.createElement('tr');
    for(j = 0; j < ((tabs.length % 2 !== 0 && (i+1) == Math.round(tabs.length/2)) ? 1 : 2); j++) {
      td = document.createElement('td');
      td.id = 'display'+x;
      td.align = "center";
      tr.appendChild(td);
      overlayContainer = document.createElement('div');
      overlayContainer.classList.add('overlayContainer');
      overlayContainer.innerHTML = '<div class="overlay" onclick="buttonClick(\'tab'+x+'\', true)"></div>';
      td.appendChild(overlayContainer);
      td.appendChild(document.getElementById('content'+x));
      x++
    }
    tbody.appendChild(tr);
  }
}
//show tab display
function showTabDisplay(show) {
  if(show) {
    for(i = 0; i < tabs.length; i++) {
      document.getElementById('content'+(i+2)).classList.add('displays');
      document.getElementById('ifrm'+(i+2)).setZoomFactor(0.5);
    }
  } else {
    for(i = 0; i < tabs.length; i++) {
      document.getElementById('content'+(i+2)).classList.remove('displays');
      document.getElementById('ifrm'+(i+2)).setZoomFactor(1);
    }
  }
}
//remove the tab display by moving all the content to a seperate div and emptying the display div
function removeTabDisplay() {
  for(i = 0; i < tabs.length; i++) {
    document.getElementById('content-container').appendChild(document.getElementById('content'+(i+2)));
  }
  var tabDisplayElement = document.getElementById('tabDisplay');
  for(i = 0; i < tabDisplayElement.children.length; i++) {
    tabDisplayElement.removeChild(tabDisplayElement.children[0]);
  }
}
//delete a tab
function deleteTab(tabNo) {
  //switch tabs (buttons)
  var element = document.getElementById('tab1');
  var oldButton = document.getElementsByClassName('active')[0];
  var button = element.children[0];
  var oldImage = oldButton.children[0];
  var image = button.children[0];
  oldButton.classList.remove('active');
  button.classList.add('active');
  oldImage.classList.remove('activeTab');
  oldImage.classList.add('tabImg');
  image.classList.remove('tabImg');
  image.classList.add('activeTab');
  //switch tab to home if the current tab is being deleted
  if(tabNo == currentTab) {
    switchContent('home', 'tab1');
    //modify title header
    var header = document.getElementById('header').children[0];
    var title = document.getElementById('tab1').children[0];
    header.innerHTML = title.title;
  }
  var generatedTabs = document.getElementsByClassName('generatedTabs');
  //update tabs array and remove deleted content/tab
  document.getElementById('content'+tabNo).remove();
  document.getElementById('tab'+tabNo).remove();
  for(i = tabNo-1; i < tabs.length; i++) {
    tabs['tab'+(i+1)] = tabs['tab'+(i+2)];
    document.getElementById('content'+(i+2)).setAttribute('id', 'content'+(i+1));
    document.getElementById('ifrm'+(i+2)).setAttribute('id', 'ifrm'+(i+1));
    document.getElementById('tab'+(i+2)).children[0].setAttribute('onclick', 'buttonClick(\'tab'+(i+1)+'\', true)');
    document.getElementById('tab'+(i+2)).setAttribute('id', 'tab'+(i+1));
  }
  delete tabs['tab'+(tabs.length+1)];
  tabs.length -= 1;
  //update tab display
  removeTabDisplay();
  generateTabDisplay();
  document.getElementById('tabDisplay').appendChild(tabDisplay);
}
//get domain from url
function getDomain(url) {
  return url.replace('http://','').replace('https://','').split(/[/?#]/)[0];
}
//get text from a file
function getFileText(filePath) {
  var result;
  var request = new XMLHttpRequest();
  request.open('GET', filePath);
  request.onreadystatechange = function() {
    result = request.responseText;
  }
  request.send();
  request.addEventListener('load', function() {
    webviewPreload = result;
  });
}
