version=$(jq '.version' package.json)
text=$(jq '.versionText' update.json)
branch=$(git rev-parse --abbrev-ref HEAD)
repo_full_name=$(git config --get remote.origin.url | sed 's/.*:\/\/github.com\///;s/.git$//')
generate_post_data()
{
  cat <<EOF
{
  "tag_name": "$version",
  "target_commitish": "$branch",
  "name": "$version",
  "body": "$text",
  "draft": false,
  "prerelease": false
}
EOF
}
if [ "$TRAVIS_OS_NAME" = "linux" ]; then
  if [ "$(jq '.version' package.json)" != "$(jq '.old_version' package.json)" ]; then
    curl --data "$(generate_post_data)" "https://api.github.com/repos/$repo_full_name/releases?access_token=$GITHUBTOKEN"
    curl --header "Content-Type:application/vnd.debian.binary-package" --data-binary "@release-builds/*.deb" "https://api.github.com/repos/$repo_full_name/releases/latest/assets?access_token=$GITHUBTOKEN" >/dev/null 2>&1
  fi
elif [ "$TRAVIS_OS_NAME" = "osx" ]; then
  if [ "$(jq '.version' package.json)" != "$(jq '.old_version' package.json)" ]; then
    curl --header "Content-Type:application/octet-stream" --data-binary "@release-builds/*.dmg" "https://api.github.com/repos/$repo_full_name/releases/latest/assets?access_token=$GITHUBTOKEN" >/dev/null 2>&1
  fi
fi
