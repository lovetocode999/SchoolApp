// Modules to control application life and create native browser window
const {app, BrowserWindow, ipcMain} = require('electron')
//extra modules
const shell = require('child_process')
const https = require('https')
const path = require('path')
const fs = require('fs')

//update application, if needed
//download json version file from github
// const newUpdate = fs.createWriteStream("newUpdate.json")
// const jsonRequest = https.get('https://raw.githubusercontent.com/lovetocode999/SchoolApp/master/update.json', function(response) {
//   response.pipe(newUpdate)
// });
// setTimeout(function() {
//   //check if the installed version matches the current version
//   var currentVersionString = require(path.join(__dirname, 'newUpdate.json')).versionString
//   var currentVersionNo = require(path.join(__dirname, 'newUpdate.json')).versionNo
//   var installedVersionString = require('./update.json').versionString
//   if(currentVersionString !== installedVersionString) {
//     //if the versions do not match, then reinstall the application
//     const homePath = process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE
//     if(process.platform == 'linux'){
//       //linux
//       //download deb file
//       const installer = fs.createWriteStream(path.join(homePath, 'SchoolAppInstall.deb'))
//       const installerRequest = https.get('https://github.com/lovetocode999/SchoolApp/releases/download/'+currentVersionString+'/schoolapp_'+currentVersionNo+'_amd64.deb', function(response) {
//         response.pipe(installer)
//       })
//       //install deb file with dpkg
//       installChild = shell.execFileSync('sudo', ['dpkg', '-i', '~/SchoolAppInstall.deb'], {stdio: 'inherit'})
//       installChild.on('exit', function() {
//         //restart application
//         app.relaunch({args: process.argv.slice(1).concat(['--relaunch'])})
//         app.exit(0)
//       })
//     } else if(process.platform == 'darwin') {
//       //osx
//       //no osx release yet
//     } else if(process.platform == 'win32') {
//       //windows
//       //download msi installer
//       const installer = fs.createWriteStream(path.join(homepath, 'SchoolAppInstall.msi'))
//       const installerRequest = https.get('https://github.com/lovetocode999/SchoolApp/releases/download/'+currentVersionString+'/SchoolApp-'+currentVersionNo+'-setup.msi', function(response) {
//         response.pipe(installer)
//       })
//       //start msi installer
//       installChild = shell.execFileSync('msiexec', ['/a', path.join(homePath, 'SchoolAppInstall.msi')], {stdio: 'inherit'})
//       installChild.on('exit', function() {
//         //restart application
//         app.relaunch({args: process.argv.slice(1).concat(['--relaunch'])})
//         app.exit(0)
//       })
//     }
//   }
// }, 200)

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    backgroundColor: '#7692bf',
    width: 800,
    height: 600,
    webPreferences: {
      webviewTag: true,
      nodeIntegration: true
    },
    icon: path.join(__dirname, 'icon64x64.png')
  })

  mainWindow.setFullScreen(true);

  // and load the index.html of the app.
  mainWindow.loadFile('index.html')

  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

//show window once tabs are loaded
ipcMain.on('application-loaded', function(event) {
})
