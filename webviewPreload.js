try {
  //embed jquery in the webview
  // var jq = document.createElement('script');
  // jq.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
  // document.getElementsByTagName('head')[0].appendChild(jq);
  var a = document.getElementsByTagName('a');
  var forms = document.getElementsByTagName('form');
  //replace all anchor targets
  for(var i=0; i<a.length; i++) {
    a[i].target = '_self';
  }
  //replace all form targets
  for(var i=0; i<forms.length; i++) {
    forms[i].target = '_self';
  }
  document.body.style.backgroundColor = "rgb(163, 194, 245)";

  //run jquery specific code after jquery has loaded
  // jq.onload = function() {
  //   //theme google classroom to match the application
  //   if(getDomain(window.location.href) == 'classroom.google.com') {
  //     $('.QRiHXd').css('background-color', 'rgb(50, 129, 255)');
  //     $('.XuQwKc').css('filter', 'brightness(0)');
  //     $('.yEedwb').css('filter', 'brightness(0)');
  //     $('.gb_Ve').css('filter', 'brightness(0)');
  //     $('.g3VIld').css('background-color', 'rgb(163, 194, 245)');
  //   }
  // }

  //get domain from url
  function getDomain(url) {
    return url.replace('http://','').replace('https://','').split(/[/?#]/)[0];
  }
}
catch(error) {
  console.log(error);
  console.error(error);
}
