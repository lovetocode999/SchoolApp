# SchoolApp

[![Greenkeeper badge](https://badges.greenkeeper.io/lovetocode999/SchoolApp.svg)](https://greenkeeper.io/)
[![Build Status](https://travis-ci.com/lovetocode999/SchoolApp.svg?branch=master)](https://travis-ci.com/lovetocode999/SchoolApp)

Just a ~~simple~~* app I'm making for doing my school next semester :smile:

*<i>Not anymore</i>
## How to Run
Prerequisites: npm and git
<pre lang="bash">
git clone https://github.com/lovetocode999/SchoolApp.git
cd SchoolApp
npm install && npm start
</pre>
<i>Note: These commands should work on most *nix type OSes, Windows OS, and MacOS</i>

Or, of course, you can download the latest release from the release page [here](https://github.com/lovetocode999/SchoolApp/releases)
